# The-Insert-Method-in-Android
//...
SQLiteDatabase db = mOpenHelper.getWritableDataBase();
      long rowId = db.insert(Notes_TABLE_NAME, Notes.NOTE, values);
      if(rowId > 0) {
        Uri noteUri = ContentUris.withAppendedId(
 NotePad.Notes.CONTENT_URI, rowId);
             getContext().getContentResolver().notifyChange(noteUri, null);
             return noteUri;
        }
             
